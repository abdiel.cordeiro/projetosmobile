import 'package:projetoaula7/contato.dart';
import 'package:flutter/material.dart';
import 'ItemContato.dart';
import 'formulario.dart';

class ListaContato extends StatefulWidget {
  final List<Contato> _contato = List();

  @override
  State<StatefulWidget> createState() {
    return ListaContatosState();
  }
}

class ListaContatosState extends State<ListaContato> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contato'),
      ),
      body: ListView.builder(
        itemCount: widget._contato.length,
        itemBuilder: (context, indice) {
          final contato = widget._contato[indice];
          return ItemContato(contato);
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          final Future<Contato> future =
              Navigator.push(context, MaterialPageRoute(builder: (context) {
            return Formulario();
          }));
          future.then((contatoRecebido) {
            debugPrint('chegou no then do future');
            debugPrint('$contatoRecebido');
            widget._contato.add(contatoRecebido);
          });
        },
      ),
    );
  }
}
