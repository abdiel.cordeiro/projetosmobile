import 'package:flutter/material.dart';
import 'contato.dart';

class Formulario extends StatelessWidget {
  final campoNome = TextEditingController();
  final campoEmail = TextEditingController();
  final campoTelefone = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Formulario'),
        ),
        body: SingleChildScrollView(
            child: Column(children: <Widget>[
          Padding(
              padding: const EdgeInsets.all(24.0),
              child: TextField(
                  controller: campoNome,
                  style: TextStyle(
                    fontSize: 24.0,
                  ),
                  decoration: InputDecoration(
                    labelText: 'Nome Completo',
                    icon: Icon(Icons.account_box),
                  ))),
          Padding(
              padding: const EdgeInsets.all(24.0),
              child: TextField(
                controller: campoEmail,
                style: TextStyle(
                  fontSize: 24.0,
                ),
                decoration: InputDecoration(
                  labelText: 'E-mail',
                  icon: Icon(Icons.mail),
                ),
                keyboardType: TextInputType.emailAddress,
              )),
          Padding(
              padding: const EdgeInsets.all(24.0),
              child: TextField(
                controller: campoTelefone,
                style: TextStyle(
                  fontSize: 24.0,
                ),
                decoration: InputDecoration(
                  labelText: 'Telefone',
                  icon: Icon(Icons.phone),
                ),
                keyboardType: TextInputType.phone,
              )),
          RaisedButton(
            child: Text('Enviar',
                style: TextStyle(color: Colors.white, fontSize: 20.0)),
            color: Colors.blue,
            highlightColor: Colors.yellow[300],
            onPressed: () => _criaContato(context),
          )
        ])));
  }

  void _criaContato(BuildContext context) {
    final String nome = campoNome.text;
    final String email = campoEmail.text;
    final String telefone = campoTelefone.text;

    if (nome != null && email != null) {
      final contatoCriado = Contato(nome, email, telefone);
      debugPrint('Criando transferência');
      debugPrint('$contatoCriado');
      Navigator.pop(context, contatoCriado);
    }
  }
}
