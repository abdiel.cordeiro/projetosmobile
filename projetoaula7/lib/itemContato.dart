import 'package:flutter/material.dart';
import 'contato.dart';

class ItemContato extends StatelessWidget {
  final Contato _contato;
  ItemContato(this._contato);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Icons.account_circle),
        title: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(_contato.nome),
            ),
            Align(
              alignment: Alignment.topLeft,
            child: Text(_contato.email)
            )
          ],
        ),
        subtitle: Text(_contato.telefone),
      ),
    );
  }
}
