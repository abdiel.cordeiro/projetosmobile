import 'main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'formulario.dart';
import 'contato.dart';

class Inicio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de usuários'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.assignment_ind),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Contato()));
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Card(
                child: const ListTile(
              leading: CircleAvatar(
                  radius: 30.0, backgroundImage: AssetImage('img/perfil.jpeg')),
              title: Text('Abdiel Cordeiro'),
              subtitle: Text('Descrição...'),
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Formulario()));
          }),
    );
  }
}
