import 'package:flutter/material.dart';

import 'my_flutter_app_icons.dart';

void main() => runApp(Calcular());

class Calcular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Calcular o IMC',
              style: TextStyle(
                fontSize: 25,
                fontFamily: 'Times new Roman',
                fontWeight: FontWeight.bold,
                color: Colors.black38,
              )),
          backgroundColor: Colors.blueGrey[300],
          leading: Icon(
            Icons.all_inclusive,
            color: Colors.purple,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Formulario(),
            ],
          ),
        ),
      ),
    );
  }
}

class Formulario extends StatelessWidget {
  final _campoNome = TextEditingController();
  final _campoEmail = TextEditingController();
  final _campoAltura = TextEditingController();
  final _campoPeso = TextEditingController();
  final _campoSexo = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            controller: _campoNome,
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Nome',
              icon: Icon(Icons.account_box),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            controller: _campoEmail,
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Email',
              icon: Icon(Icons.email),
            ),
            keyboardType: TextInputType.emailAddress,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            controller: _campoSexo,
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Sexo',
              icon: Icon(Icons.wc),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            controller: _campoPeso,
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Peso',
              icon: Icon(Icons.fastfood),
            ),
            keyboardType: TextInputType.number,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            controller: _campoAltura,
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Altura',
              icon: Icon(Icons.file_upload),
            ),
            keyboardType: TextInputType.number,
          ),
        ),
        RaisedButton(
          child: Text("Calcular",
              style: TextStyle(color: Colors.white, fontSize: 40.0)),
          color: Colors.green[100],
          highlightColor: Colors.yellow[300],
          onPressed: () {
            final String nome = _campoNome.text;
            final String email = _campoEmail.text;
            final String sexo = _campoSexo.text;
            final String peso = _campoPeso.text;
            final String altura = _campoAltura.text;

            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return Calculo(nome, email, sexo, peso, altura);
                });
          },
        )
      ],
    );
  }
}

class Calculo extends StatelessWidget {
  final String nome;
  final String email;
  final String sexo;
  final String peso;
  final String altura;

  Calculo(this.nome, this.email, this.sexo, this.peso, this.altura);

  String CalcularIMC(String peso, String altura) {
    double calculo = int.parse(peso) / (double.parse(altura) * double.parse(altura));
    return "Seu IMC: " + calculo.toStringAsPrecision(4);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Sucesso'),
      content: Text('Nome: $nome' +
          '\nEmail: $email' +
          '\nSeu sexo é: $sexo' +
          '\nSeu peso é: $peso' +
          '\nSua Altura é: $altura' +
          '\n' + CalcularIMC('$peso', '$altura')
          ),
      actions: <Widget>[
        FlatButton(
          child: new Text("Fechar"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}