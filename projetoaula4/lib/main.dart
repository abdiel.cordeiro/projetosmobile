import 'package:flutter/material.dart';

import 'my_flutter_app_icons.dart';

void main() => runApp(Calcular());

class Calcular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Calcular o IMC',
              style: TextStyle(
                fontSize: 25,
                fontFamily: 'Times new Roman',
                fontWeight: FontWeight.bold,
                color: Colors.black38,
              )),
          backgroundColor: Colors.orange[200],
          leading: Icon(
            Icons.all_inclusive,
            color: Colors.purple,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Formulario(),
              BotaoCalcular(),
            ],
          ),
        ),
      ),
    );
  }
}

class Formulario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Nome',
              icon: Icon(Icons.account_box),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Telefone',
              icon: Icon(Icons.phone),
            ),
            keyboardType: TextInputType.phone,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Sexo',
              icon: Icon(Icons.wc),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: TextField(
            style: TextStyle(
              fontSize: 24.0,
            ),
            decoration: InputDecoration(
              labelText: 'Peso',
              icon: Icon(MyFlutterApp.balance_scale),
            ),
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }
}

class BotaoCalcular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text("Calcular",
          style: TextStyle(color: Colors.white, fontSize: 20.0)),
      color: Colors.blue,
      highlightColor: Colors.yellow[300],
      onPressed: () {},
    );
  }
}
