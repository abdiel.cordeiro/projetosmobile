import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
    home: Scaffold(
        body: ListaProdutos(),
        appBar: AppBar(
          title: Text(
            "Cordeiro's Shop",
            style: TextStyle(
              fontSize: 25,
              fontFamily: 'Times new Roman',
              fontWeight: FontWeight.bold,
              color: Colors.black38,
            ),
          ),
          backgroundColor: Colors.orange[200],
          leading: Icon(
            Icons.all_inclusive,
            color: Colors.purple,
          ),
        ))));

class Produto extends StatelessWidget {
  Produto(this.icone, this.titulo, this.subTitulo);
  final IconData icone;
  final String titulo;
  final String subTitulo;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(
          this.icone,
          color: Colors.black,
          size: 30.0,
          semanticLabel: this.titulo,
        ),
        title: Text(this.titulo,
            style: TextStyle(
              fontSize: 15,
              fontFamily: 'Times new Roman',
              fontWeight: FontWeight.bold,
              color: Colors.black87,
            )),
        subtitle: Text(this.subTitulo),
      ),
    );
  }
}

class ListaProdutos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Produto(Icons.person, 'Perfil', 'Informações do Usuário'),
        Produto(Icons.store, 'Loja', 'Todas as roupas disponiveis'),
        Produto(Icons.add_shopping_cart, 'Roupas no carrinho', 'Todas as suas compras'),
        Produto(Icons.settings, 'Configuraçoes', 'Informaçõe do aplicativo')
      ],
    );
  }
}
