import 'package:aula8/ListaContato.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'contato.dart';
import 'app_database.dart';

class ItemContato extends StatelessWidget {
  final Contato _contato;
  ItemContato(this._contato);

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: AlignmentDirectional.bottomEnd, children: <Widget>[
      Card(
        child: ListTile(
          leading: iconAgenda,
          title: Text(_contato.nome),
          subtitle: Text(_contato.email +
              " - " +
              _contato.telefone +
              "\n" +
              _contato.endereco),
          isThreeLine: true,
        ),
      ),
      FlatButton(
        onPressed: () {
          delete(_contato.id);
          Navigator.of(context)
              .push(new MaterialPageRoute(builder: (BuildContext context) {
            return new ListaContato();
          }));
        },
        child: ListTile(
          leading: iconDelete,
        ),
      )
    ]);
  }
}

Image iconAgenda = new Image(
    image: new ExactAssetImage("assets/imagem/user.png"),
    height: 20.0,
    width: 20.0,
    alignment: FractionalOffset.center);

    
Image iconDelete = new Image(
    image: new ExactAssetImage("assets/imagem/delete.png"),
    height: 20.0,
    width: 20.0,
    alignment: FractionalOffset.center);
