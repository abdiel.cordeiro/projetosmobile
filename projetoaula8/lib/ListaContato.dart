import 'package:aula8/Formulario.dart';
import 'package:aula8/app_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'contato.dart';
import 'ItemContato.dart';

class ListaContato extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Contatos",
              style: TextStyle(
                fontSize: 25,
                fontFamily: 'Times new Roman',
                fontWeight: FontWeight.bold,
                color: Colors.white,
              )),
          backgroundColor: Colors.red),
      body: FutureBuilder(
        future: findAll(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              break;
            case ConnectionState.waiting:
              return ProgressCirc();
              break;
            case ConnectionState.active:
              break;
            case ConnectionState.done:
              final List<Contato> contacts = snapshot.data;
              return ListView.builder(itemCount: contacts.length, itemBuilder: (context, index){
                final Contato contato = contacts[index];
                return ItemContato(contato);
              });
              break;
          }
          return null;
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Formulario()),
          );
        },
      ),
    );
  }
}

class ProgressCirc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        Text('Loading'),
      ],
    ));
  }
}
