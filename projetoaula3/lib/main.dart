import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: Scaffold(
        body: Card(
          child: ListTile(
            leading: Icon(
              Icons.favorite_border,
              color: Colors.red,
              size: 50.0,
              semanticLabel: 'texto alternativo para leitores de tela',
            ),
            title: Text('Perfil',
                style: TextStyle(
                  fontSize: 25,
                  fontFamily: 'Arial',
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                )),
            subtitle: Text('Informações do perfil'),
          ),
        ),
        appBar: AppBar(
          title: Text('App Aula 03'),
        ),
      ),
    ));
